package nam;

import java.awt.Color;

public class BackgroundColorCommand implements ICommand {
	
	private View view;
	private Color oldColor;
	private Color newColor;
	
	public BackgroundColorCommand(View view, Color color) {
		this.view = view;
		this.newColor = color;
	}

	@Override
	public void doExecute() {
		oldColor = view.getBackgroundColor();
		view.setBackgroundColor(newColor);
	}

	@Override
	public void undoExecute() {
		view.setBackgroundColor(oldColor);
	}

}
