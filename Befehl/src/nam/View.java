package nam;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class View extends JFrame {
	
	private JMenu editMenu;
	private JMenu backgroundColorMenu;

	public View(String title) {
		super(title);
		
		JMenuBar menuBar = new JMenuBar(); 
		editMenu = new JMenu( "Bearbeiten" ); 
		backgroundColorMenu = new JMenu( "Hintergrundfarben" ); 
		menuBar.add( editMenu ); 
		menuBar.add( backgroundColorMenu ); 
		this.setJMenuBar( menuBar );
		
		
		JMenuItem undoItem = new JMenuItem("Undo");
		editMenu.add(undoItem);
		undoItem.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	//UNDO
		    	StackCommands.getInstance().undoCommand();
		    }
		});
		
		JMenuItem blackItem = new JMenuItem("Schwarz");
		blackItem.addActionListener(new CommandListener(new BackgroundColorCommand(this, new Color(0,0,0))));
		backgroundColorMenu.add(blackItem);
		JMenuItem whiteItem = new JMenuItem("Wei�");
		whiteItem.addActionListener(new CommandListener(new BackgroundColorCommand(this, new Color(255,255,255))));
		backgroundColorMenu.add(whiteItem);
		JMenuItem redItem = new JMenuItem("Rot");
		redItem.addActionListener(new CommandListener(new BackgroundColorCommand(this, new Color(255,0,0))));
		backgroundColorMenu.add(redItem);
		JMenuItem greenItem = new JMenuItem("Gr�n");
		greenItem.addActionListener(new CommandListener(new BackgroundColorCommand(this, new Color(0,192,0))));
		backgroundColorMenu.add(greenItem);
		JMenuItem blueItem = new JMenuItem("Blau");
		blueItem.addActionListener(new CommandListener(new BackgroundColorCommand(this, new Color(0,0,255))));
		backgroundColorMenu.add(blueItem);
		JMenuItem yellowItem = new JMenuItem("Gelb");
		yellowItem.addActionListener(new CommandListener(new BackgroundColorCommand(this, new Color(255,255,0))));
		backgroundColorMenu.add(yellowItem);
		
		
	}
	
	public void setBackgroundColor(Color newColor) {
		this.getContentPane().setBackground(newColor);
	}	
	public Color getBackgroundColor() {
		return this.getContentPane().getBackground();
	}


}
