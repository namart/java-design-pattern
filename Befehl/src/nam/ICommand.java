package nam;

public interface ICommand {
	public void doExecute();
	public void undoExecute();
}
