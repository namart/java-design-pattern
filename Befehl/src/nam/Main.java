package nam;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		View view = new View("Command Pattern");
		view.pack();
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setSize(900, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		view.setLocation((d.width - view.getSize().width) / 2,
				(d.height - view.getSize().height) / 2);
		view.setVisible(true);

	}

}
