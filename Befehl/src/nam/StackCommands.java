package nam;

import java.util.Stack;


public class StackCommands {
	
	private Stack<ICommand> commands;

	//SingletonClass
	private static final StackCommands instance = new StackCommands();
	public static StackCommands getInstance() {
		return instance;
	}
	
	public StackCommands() {
		commands = new Stack<ICommand>();
	}
	
	public void addCommand(ICommand command) {
		commands.push(command);
	}
	
	public void undoCommand() {
		if(!commands.isEmpty()) {
			commands.pop().undoExecute();
		}
	}
}
