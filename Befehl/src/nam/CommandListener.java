package nam;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CommandListener implements ActionListener {
	ICommand command = null;
	
	public CommandListener(ICommand command) {
		this.command = command;
	}
	
	public ICommand getCommand() {
		return command;
	}
	
	public void actionPerformed(ActionEvent e) {
		StackCommands.getInstance().addCommand(command);
        command.doExecute();
	}

}
