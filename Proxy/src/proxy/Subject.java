package proxy;

public interface Subject {

		String getData();
}
