package proxy;

public class ProxyClient implements Subject {
	
	private RealClient client;
	private String ID = "1234";
	private String password = "password";
	private boolean isValid = false;

	@Override
	public String getData() {

		if(isValid) {
			if (client == null) {
				client = new RealClient();
	        } 
			return client.getData();
		}
		return "username/password not valid";
	}
	
	public boolean authenticate(String id, String password) {
		
		if(this.ID == id && this.password == password) {
			this.isValid = true;
		}
		
		return this.isValid;
	}

}
