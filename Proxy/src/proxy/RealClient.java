package proxy;

public class RealClient implements Subject {

	@Override
	public String getData() {
		
		return "Information";
	}

}
