package proxy;

public class Proxy {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//besser mit SecurityManager damit der User nicht mitgeschickt werden muss
		
		String msg;
		
		ProxyClient client = new ProxyClient();
		msg = client.getData();
		System.out.println(msg);
		
		client.authenticate("1234", "password");
		msg = client.getData();
		
		System.out.println(msg);

	}

}
