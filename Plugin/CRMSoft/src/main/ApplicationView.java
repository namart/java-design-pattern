package main;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class ApplicationView extends JFrame {
	
	private JTabbedPane tabbedPane;

	public ApplicationView(String title) {
		
		super(title);
		
		this.setLayout(new BorderLayout());
		
		
		tabbedPane = new JTabbedPane();
		add(tabbedPane, BorderLayout.CENTER);

	}
	
	public void addTab(String title, JPanel component) {
		tabbedPane.addTab(title, component);
	}
}
