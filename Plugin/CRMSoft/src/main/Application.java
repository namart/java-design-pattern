package main;


import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import interfaces.IPlugin;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//W�hle Plugins zum laden
		List<String> plugins = new ArrayList<String>();
		plugins.add("plugin.FirstPlugin");
		plugins.add("plugin.SecondPlugin");
		
		PluginManager PM = new PluginManager();
		try {
			PM.search("Plugins", plugins);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<IPlugin> ap = PM.getPluginCollection();
		
		ApplicationView view = new ApplicationView("EPU-Backoffice");
		view.pack();
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setSize(500, 200);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		view.setLocation((d.width - view.getSize().width) / 2,
				(d.height - view.getSize().height) / 2);
		view.setVisible(true);
		
		for(IPlugin i : ap) {
			System.out.println(i.getName());
			view.addTab(i.getName(), i.getView());
			i.performAction("a Test" + i);
		}
		
	}

}
