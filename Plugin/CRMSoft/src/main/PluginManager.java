package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import interfaces.IPlugin;

public class PluginManager {
	
	private ArrayList<IPlugin> pluginCollection;
	
	public PluginManager() {
		
		pluginCollection = new ArrayList<IPlugin>();
	}

	public void search(String directory, List<String> classNames) throws Exception {
		File dir = new File(directory);
		if (dir.isFile()) 
			return;
		
		File[] files = dir.listFiles(new JarFilter());
		for (File f : files) {

			for (String className : classNames) {
				
				System.out.println("File: " + f + "Klasse: " + className);

				Class<?> clazz = getClass(f, className);
				
				if(clazz != null) {
					Class<?>[] interfaces = clazz.getInterfaces();
					for (Class<?> c : interfaces) {
						// Implement the IPlugin interface
						if (c.getName().equals("interfaces.IPlugin")) {
							pluginCollection.add((IPlugin)clazz.newInstance());
						}
					}
				}
			}
		}
	}
	
	public Class<?> getClass(File file, String name) throws Exception {
	
		URLClassLoader classLoader;
		Class<?> clazz = null;
		classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()});
		
		try {
			clazz = classLoader.loadClass(name);	
		} 
		catch ( ClassNotFoundException ex)  {
			System.out.println("Invalid Plugin File: " + name );    				  
		} 

		return clazz;
	}
	
	public ArrayList<IPlugin> getPluginCollection() {
		return pluginCollection;
	}

	public void setPluginCollection(ArrayList<IPlugin> pluginCollection) {
		this.pluginCollection = pluginCollection;
	}
}

class JarFilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		return (name.endsWith(".jar"));
	}
}