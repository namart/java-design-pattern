package interfaces;

import javax.swing.JPanel;

public interface IPlugin {

	String getName();

	void performAction(String context);
	
	JPanel getView();
}
