package plugin;

import javax.swing.JPanel;
import interfaces.IPlugin;


public class FirstPlugin implements IPlugin {

	@Override
	public String getName() {

		return "Erstes Plugin";
	}

	@Override
	public void performAction(String arg0) {

		System.out.println(arg0);
	}

	@Override
	public JPanel getView() {
		return new FirstPluginView();
	}

}
