package plugin;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FirstPluginView extends JPanel {
	
	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	
	//Buttons
	protected JButton saveButton = new JButton("Speichern");
	protected JButton deleteButton = new JButton("L�schen");

	public FirstPluginView() {
		
		
		this.setLayout( new BoxLayout(this, BoxLayout.Y_AXIS) );
		this.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		this.setSize(500, 50);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(2,2) );
		this.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		
		//Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		
		this.saveButton.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	fieldNr.setText("1");
		    	System.out.println("Save button clicked");
		    }
		});
		
		this.deleteButton.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	fieldNr.setText(null);
		    	fieldName.setText(null);
		    	System.out.println("Delete button clicked");
		    }
		});
	}

	
}
