package myadapter;

import java.net.NetworkInterface;

public class MyNetworkInterface implements Comparable<MyNetworkInterface>
{
	private NetworkInterface nwInt = null;	
	public NetworkInterface getNwInt() 
	{
		return nwInt;
	}

	public MyNetworkInterface(NetworkInterface extNwInt)
	{
		nwInt = extNwInt;
	}


	@Override
	public int compareTo(MyNetworkInterface o)
	{
		return (nwInt.getName().compareTo(o.nwInt.getName()));
	}
	
	public String toString()
	{
		return nwInt.getName();
	}
	
}
