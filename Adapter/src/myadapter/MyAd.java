package myadapter;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;


public class MyAd
{
	public MyAd()
	{
		
	}
	
	public static void main(String[] args)
	{
		NetworkInterfacesCollector nwICollector = new NetworkInterfacesCollector();
		ArrayList<MyNetworkInterface> networks = nwICollector.getNetworkInterfaces();
	    MyNetworkInterface nArray[] = new MyNetworkInterface[networks.size()];
		
	    Collections.sort(networks);
	    
	    for (int i=0; i<networks.size(); i++)
	    {
	    	nArray[i] = networks.get(i);
	    }

		JComboBox networkBox = new JComboBox(nArray);
		
		MyWin window = new MyWin();
		window.add(networkBox);
		window.setVisible(true);
	}
	
}