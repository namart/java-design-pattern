package myadapter;

import javax.swing.JFrame;

public class MyWin extends JFrame
{
	private static final long serialVersionUID = 1L;

	public MyWin()
	{
		this.setSize(640, 360);
		this.setResizable(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}