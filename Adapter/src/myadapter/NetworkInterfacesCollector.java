package myadapter;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

public class NetworkInterfacesCollector
{

	public NetworkInterfacesCollector()
	{

	}
	
	public ArrayList<MyNetworkInterface> getNetworkInterfaces()
	{
		ArrayList<MyNetworkInterface> networks = new ArrayList<MyNetworkInterface>();
		
		try 
		{
			Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces();
			
		    while ( niEnum.hasMoreElements() ) 
		    {
		    	networks.add(new MyNetworkInterface(niEnum.nextElement()));
		    }
		    
		    for(MyNetworkInterface ni : networks)
		    {
		    	System.out.println(ni.toString());
		    }

		} 
		catch (SocketException e)
		{
			e.printStackTrace();
		}
		
		return networks;
	}
}
