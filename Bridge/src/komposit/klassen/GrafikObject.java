package komposit.klassen;

import java.awt.Graphics;

import javax.swing.JPanel;

public interface GrafikObject {

	void draw(Graphics g);
	void size(int width, int height);
	void move(int x, int y);
}
