package bridge.klassen;


/* Spezialisierte Abstraktion */
public class GeoShapes extends Shape {
	
	private int width = 1000;
	private int height = 1000;
	private int x = 0;
	private int y = 0;
	private DrawingAPI drawingAPI;

	public GeoShapes(DrawingAPI drawingAPI) {
		
		this.drawingAPI = drawingAPI;
		
		setBounds( 0, 0, width, height );
		setOpaque(false);
		
		add(drawingAPI);
	}

	@Override
	public void setContentSize(int newWidth, int newHeight) {
		setBounds( x, y, newWidth, newHeight );
	}

	@Override
	public void setContentPosition(int newX, int newY) {
		setBounds( newX, newY, width, height );
	}

	@Override
	public void switchObjectTo(DrawingAPI object, int index) {
		this.remove(getGraphicObjectByIndex(index));
		this.add(object, index);
		this.repaint();
	}

	@Override
	public void addObject(DrawingAPI object) {
		add(object);
	}

	
}
