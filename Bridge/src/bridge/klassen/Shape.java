package bridge.klassen;

import komposit.klassen.VerbundGrafikObjekt;

/* Abstraktion */
public abstract class Shape extends VerbundGrafikObjekt {

	public abstract void setContentSize(int width, int height);
	public abstract void setContentPosition(int x, int y);
	public abstract void switchObjectTo(DrawingAPI object, int index);
	public abstract void addObject(DrawingAPI object);
}
