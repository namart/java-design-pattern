package bridge.klassen;

import komposit.klassen.GrafikObject;

/* Implementierer */
public interface DrawingAPI extends GrafikObject {

	int getX();
	void setX(int x);
	int getY();
	void setY(int y);
	int getWidth();
	void setWidth(int width);
	int getHeight();
	void setHeight(int height);
}
