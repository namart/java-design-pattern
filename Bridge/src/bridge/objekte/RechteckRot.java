package bridge.objekte;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import bridge.klassen.DrawingAPI;

/* Konkreter Implementierer */
public class RechteckRot implements DrawingAPI {
	
	private int x;
	private int y;
	private int width;
	private int height;

	public RechteckRot(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void draw(Graphics g) {

		//System.out.println("rechteck rot wird gezeichnet");
		
		//Farbe
		g.setColor(new Color(255,0,0));
		g.drawRect(x, y, width, height);
	}
	
	public void size(int width, int height) {
		this.width += width;
		this.height += height;
	}
	
	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}
	
	/*Getter & Setter*/
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
