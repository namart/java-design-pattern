package bridge.objekte;

import java.awt.Graphics;

import bridge.klassen.DrawingAPI;

/* Konkreter Implementierer */
public class Dreieck implements DrawingAPI {
	
	private int x;
	private int y;
	private int width;
	private int height;

	public Dreieck(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void draw(Graphics g) {
		
		//System.out.println("dreieck wird gezeichnet");

		// Die horizontale Linie
		g.drawLine(x, y, x + width, y);

		// die anderen zwei
		g.drawLine(x, y, x + (width / 2), y + height); // links
		g.drawLine(x + (width / 2), y + height, x + width, y); // rechts
	}
	
	public void size(int width, int height) {
		this.width += width;
		this.height += height;
	}
	
	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}

	/*Getter & Setter*/
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
