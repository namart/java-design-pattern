package bridge;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import komposit.klassen.GrafikObject;

import bridge.klassen.DrawingAPI;
import bridge.klassen.GeoShapes;
import bridge.klassen.Shape;
import bridge.objekte.Dreieck;
import bridge.objekte.DreieckRot;
import bridge.objekte.Kreis;
import bridge.objekte.KreisRot;
import bridge.objekte.Rechteck;
import bridge.objekte.RechteckRot;


public class Bridge extends JFrame {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	
	public enum Themes {
	  RED, DEFAULT
	}

	public Bridge() {

		JPanel content = new JPanel();
		content.setLayout(null);
		 
		shapes.add(new GeoShapes(new Dreieck(0, 0, 100, 100)));
		shapes.add(new GeoShapes(new Rechteck(0, 0, 100, 100)));
		shapes.add(new GeoShapes(new Kreis(0, 0, 100, 100)));	
		
		shapes.get(0).add(new Dreieck(0, 0, 300, 150));
		
		int i = 110;
		for (Shape shape : shapes) {
			content.add(shape);
			shape.setContentPosition(i, i);
			i += i;
	    }

		
		
        JButton loadRedTheme = new JButton("Load Red Theme");
        loadRedTheme.setBounds(200, 0, 200, 30);
        content.add(loadRedTheme);
        loadRedTheme.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {

		    	switchTheme(Themes.RED);
		    }
		});
        
        JButton loadDefaultTheme = new JButton("Load Default Theme");
        loadDefaultTheme.setBounds(400, 0, 200, 30);
        content.add(loadDefaultTheme);
        loadDefaultTheme.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {

		    	switchTheme(Themes.DEFAULT);
		    }
		});
        
        
        setContentPane(content);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Demo Drawing");
        setLocationRelativeTo(null);  // Center window.
       
        pack();

	}
	
	private void switchTheme(Themes name) {
		
		
		switch(name) {
			
			case RED:
				
				for (Shape shape : shapes) {
					
					for(int i=0; i<shape.getChildren().size(); i++) {
						
						DrawingAPI object = (DrawingAPI) shape.getGraphicObjectByIndex(i);
						
						System.out.println(shape.getGraphicObjectByIndex(i).toString());
						
						if(shape.getGraphicObjectByIndex(i).toString().contains("Dreieck")) {
							shape.switchObjectTo(new DreieckRot(object.getX(), object.getY(), object.getWidth(), object.getHeight()), i);
						}
						
						else if(shape.getGraphicObjectByIndex(i).toString().contains("Rechteck")) {
							shape.switchObjectTo(new RechteckRot(object.getX(), object.getY(), object.getWidth(), object.getHeight()), i);
						}
						
						else if(shape.getGraphicObjectByIndex(i).toString().contains("Kreis")) {
							shape.switchObjectTo(new KreisRot(object.getX(), object.getY(), object.getWidth(), object.getHeight()), i);
						}
					}
			    }

				break;
				
				
	
			
			case DEFAULT:
				
				for (Shape shape : shapes) {
					
					for(int i=0; i<shape.getChildren().size(); i++) {
						
						DrawingAPI object = (DrawingAPI) shape.getGraphicObjectByIndex(i);
						
						System.out.println(shape.getGraphicObjectByIndex(i).toString());
						
						if(shape.getGraphicObjectByIndex(i).toString().contains("Dreieck")) {
							shape.switchObjectTo(new Dreieck(object.getX(), object.getY(), object.getWidth(), object.getHeight()), i);
						}
						
						else if(shape.getGraphicObjectByIndex(i).toString().contains("Rechteck")) {
							shape.switchObjectTo(new Rechteck(object.getX(), object.getY(), object.getWidth(), object.getHeight()), i);
						}
						
						else if(shape.getGraphicObjectByIndex(i).toString().contains("Kreis")) {
							shape.switchObjectTo(new Kreis(object.getX(), object.getY(), object.getWidth(), object.getHeight()), i);
						}
					}
			    }

				break;
				
			default: break;

			
		}
	
	}
	
	public static void main(String[] args) {
		
		Bridge f = new Bridge();
		f.setSize(900, 600);
	    f.setVisible( true );

	}

}


