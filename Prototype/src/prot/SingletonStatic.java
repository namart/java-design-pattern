package prot;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;


public final class SingletonStatic {
	
	private static final SingletonStatic instance = new SingletonStatic();

	private Properties configProp = new Properties();
	
	private ArrayList<ConcretePrototype> prototypes = new ArrayList<ConcretePrototype>();
	
	private SingletonStatic() {
		
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("config.properties");
        try {
            configProp.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        Enumeration<Object> em = configProp.keys();
        while(em.hasMoreElements()){
          String str = (String)em.nextElement();
          System.out.println(str + ": " + configProp.get(str));
          
          ConcretePrototype cp = new ConcretePrototype();
          cp.setName(str);
          cp.setValue(configProp.get(str));
          prototypes.add(cp);
        }

	}

	public ConcretePrototype findAndClone(String name) {
		for (ConcretePrototype v : prototypes){
			if (v.getName().equals(name)) {
				return v.clone();
			}
		}
		System.out.println(name + " not found");
		return null;
	}

	public static SingletonStatic getInstance() {
		return instance;
	}
	
}