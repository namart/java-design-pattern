package prot;

public class ConcretePrototype extends Prototype {
	
	private String itsName;
	private Object itsValue;

	@Override
	public ConcretePrototype clone() {
		try {
            return (ConcretePrototype) super.clone();
        } catch (CloneNotSupportedException cnse) {
            return null;
        }
	}

	public void setName(String name) {
		itsName = name;
	}
	
	public String getName() {
		return itsName;
	}

	public void setValue(Object value) {
		itsValue = value;
	}
	
	public Object getValue() {
		return itsValue;
	}
}
