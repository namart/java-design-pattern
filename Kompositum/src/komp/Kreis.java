package komp;

import java.awt.Graphics;
import java.awt.Graphics2D;

import komp.klassen.GrafikObject;


public class Kreis implements GrafikObject {

	private int x;
	private int y;
	private int width;
	private int height;
	
	public Kreis(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public void draw(Graphics g) {
		g.drawOval(x, y, width, height);
	}
	
	public void size(int width, int height) {
		this.width += width;
		this.height += height;
	}
	
	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}

}
