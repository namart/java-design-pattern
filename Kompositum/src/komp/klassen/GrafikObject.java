package komp.klassen;

import java.awt.Graphics;

public interface GrafikObject {

	void draw(Graphics g);
	void size(int width, int height);
	void move(int x, int y);
}
