package komp;

import java.awt.Graphics;

import komp.klassen.GrafikObject;

public class Dreieck implements GrafikObject {
	
	private int x;
	private int y;
	private int width;
	private int height;

	public Dreieck(int x, int y, int width, int height) {
		this.x = x;
		this.y = y-height;
		this.width = width;
		this.height = height;
	}

	@Override
	public void draw(Graphics g) {
		//g.drawRect(x, y, width, height);
		
		System.out.println("dreieck wird gezeichnet");

		// Die horizontale Linie
		g.drawLine(x, y, x + width, y);

		// die anderen zwei
		g.drawLine(x, y, x + (width / 2), y + height); // links
		g.drawLine(x + (width / 2), y + height, x + width, y); // rechts
	}
	
	public void size(int width, int height) {
		this.width += width;
		this.height += height;
	}
	
	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}

}
