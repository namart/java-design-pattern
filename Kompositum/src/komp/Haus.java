package komp;

import komp.klassen.VerbundGrafikObjekt;

public class Haus extends VerbundGrafikObjekt {

	public Haus() {
		
		setBounds( 0, 0, 1000, 1000 );
		setOpaque(false);
		
		 add(new Dreieck(10, 10, 100, 100));
	     add(new Kreis(10, 10, 100, 100));
	     add(new Rechteck(10, 10, 100, 100));
	}


}
