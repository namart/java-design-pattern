package sin;

import java.util.HashMap;

public class SingletonRegistry {

	public static SingletonRegistry REGISTRY = new SingletonRegistry();
	private static HashMap map = new HashMap();

	private SingletonRegistry() {
	}

	public static synchronized Object getInstance(String classname) {
		Object singleton = map.get(classname);

		if (singleton != null) {
			return singleton;
		}
		try {
			singleton = Class.forName(classname).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put(classname, singleton);
		return singleton;
	}
}
