package sin;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		SingletonStatic ss = SingletonStatic.getInstance();
		
		SingletonLazy sl = SingletonLazy.getInstance();
		
		System.out.println(ss.getDBConnection());
		System.out.println(sl.getDBConnection());
		System.out.println(ss.getDBConnection());
		
		ss.clodeDBConnection();
		sl.clodeDBConnection();

		System.out.println(sl.getDBConnection());
		
	}
}
