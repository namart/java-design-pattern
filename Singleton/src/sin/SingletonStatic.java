package sin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class SingletonStatic {
	private static final SingletonStatic instance = new SingletonStatic();
	private Connection conn = null;
	
	private SingletonStatic() {
		
		try 
        {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch(ClassNotFoundException ex)
        {
            System.out.println(ex.getMessage());
        }
		
		try
        {
			String userName = "root";
	        String password = "root";
	        String url = "jdbc:mysql://localhost/swe";
	        conn = DriverManager.getConnection(url, userName, password);
        }
        catch (Exception e)
        {
            System.err.println ("Cannot connect to database server");
        }
	}

	public static SingletonStatic getInstance() {
		return instance;
	}
	
	public Connection getDBConnection() {

		return this.conn;
	}
	
	public void clodeDBConnection() {
		
		try {
			this.conn.close();
			System.out.println ("Database connection terminated");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}