package state.classes;

import java.awt.Point;
import komp.classes.VerbundGrafikObjekt;

/* Kontext */
public class Cursor {
	
	private AbstractTool currentTool;
	
	public Cursor(VerbundGrafikObjekt drawingBoard) {
		this.setState(new RectTool(drawingBoard));
	}
	
	public void moveTo(Point p) {
		currentTool.moveTo(p);
	}
	
	public void mouseDown(Point p) {
		currentTool.mouseDown(p);
	}
	
	public void mouseUp(Point p) {
		currentTool.mouseUp(p);
	}
	
	public void setState(AbstractTool tool) { 
        this.currentTool = tool; 
	}

}
