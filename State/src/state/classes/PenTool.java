package state.classes;

import java.awt.Point;

import comand.classes.ICommand;
import comand.classes.PaintCommand;
import comand.classes.StackCommands;

import komp.classes.VerbundGrafikObjekt;
import state.objects.Line;
import state.objects.Object;

/* Konkreter Zustand */
public class PenTool extends AbstractTool {
	
	private Point LastMousePosition = null;
	private String MouseButton = "UP";
	private VerbundGrafikObjekt drawingBoard;
	private int group = 1;
	private Line latestItem = null;
	
	public PenTool(VerbundGrafikObjekt drawingBoard) {
		this.drawingBoard = drawingBoard;
	}

	@Override
	public void moveTo(Point p) {
		if (MouseButton == "DOWN") {

			//Draw
			Line l = new Line(LastMousePosition, p, group);
			int size = drawingBoard.getChildrenSize();
			if(size != 0) {
				
				Object temp = (Object) drawingBoard.getGraphicObjectByIndex(size-1);
				if( temp.getGroup() == group) {
					drawingBoard.remove(temp);
				}
				
			}
		
	        drawingBoard.add(l);
			drawingBoard.repaint();
	        
	        latestItem = l;
			
		}
		
	}

	@Override
	public void mouseDown(Point p) {
		MouseButton = "DOWN";
		group++;
		if(LastMousePosition == null) {
			LastMousePosition = p;
		}

	}

	@Override
	public void mouseUp(Point p) {
		MouseButton = "UP";
		LastMousePosition = p;
		
		drawingBoard.remove(drawingBoard.getLastInsert());
		ICommand command = new PaintCommand(drawingBoard, latestItem);
		StackCommands.getInstance().addCommand(command);
        command.doExecute();
	}

}
