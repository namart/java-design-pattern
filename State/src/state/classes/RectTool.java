package state.classes;

import java.awt.Point;

import comand.classes.ICommand;
import comand.classes.PaintCommand;
import comand.classes.StackCommands;

import komp.classes.VerbundGrafikObjekt;
import state.objects.Line;
import state.objects.Object;
import state.objects.Rect;

/* Konkreter Zustand */
public class RectTool extends AbstractTool {
	
	private Point LastMousePosition = null;
	private String MouseButton = "UP";
	private VerbundGrafikObjekt drawingBoard;
	private int group = 1;
	private Rect latestItem = null;
	
	public RectTool(VerbundGrafikObjekt drawingBoard) {
		this.drawingBoard = drawingBoard;
	}

	@Override
	public void moveTo(Point p) {
		if (MouseButton == "DOWN") {

			//Draw
			Rect r = new Rect(LastMousePosition, p, group);
			int size = drawingBoard.getChildrenSize();
			if(size != 0) {
				
				Object temp = (Object) drawingBoard.getGraphicObjectByIndex(size-1);
				if( temp.getGroup() == group) {
					drawingBoard.remove(temp);
				}
				
			}
			
			drawingBoard.add(r);
			drawingBoard.repaint();
	
			latestItem = r;
		}
		
	}

	@Override
	public void mouseDown(Point p) {
		MouseButton = "DOWN";
		group++;
		LastMousePosition = p;

	}

	@Override
	public void mouseUp(Point p) {
		MouseButton = "UP";
		
		drawingBoard.remove(drawingBoard.getLastInsert());
		ICommand command = new PaintCommand(drawingBoard, latestItem);
		StackCommands.getInstance().addCommand(command);
        command.doExecute();
	}

}
