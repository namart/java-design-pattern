package state.classes;

import java.awt.Point;

/* Zustand */
public abstract class AbstractTool {

	public abstract void moveTo(Point p);
	public abstract void mouseDown(Point p);
	public abstract void mouseUp(Point p);
	
}
