package state.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class Line extends Object {
	
	private Point p1;
	private Point p2;

	public Line(Point startPoint, Point endPoint, int group) {
		super(group);
		this.p1 = startPoint;
		this.p2 = endPoint;
	}

	@Override
	public void draw(Graphics g) {

		// Die horizontale Linie
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.setColor(Color.red);
	}
}
