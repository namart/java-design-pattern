package state.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class Rect extends Object {
	
	private Point p1;
	private Point p2;

	public Rect(Point startPoint, Point endPoint, int group) {
		super(group);
		this.p1 = startPoint;
		this.p2 = endPoint;
	}

	@Override
	public void draw(Graphics g) {

		int width = p2.x - p1.x;
		int height = p2.y - p1.y;
		//Rechteck
		g.setColor(Color.red);
		g.drawRect(p1.x, p1.y, width, height);
		g.fillRect(p1.x, p1.y, width, height);

	}

}
