package state.objects;

import java.awt.Graphics;
import java.awt.Point;

import komp.classes.GrafikObject;

public class Object implements GrafikObject {
	
	private int group;

	public Object(int group) {
		this.group = group;
	}

	@Override
	public void draw(Graphics g) {}

	public int getGroup() {
		return group;
	}

}
