package state;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import comand.classes.StackCommands;

import state.classes.PenTool;
import state.classes.RectTool;
import state.view.DrawingBoard;



public class State extends JFrame {

	private DrawingBoard content;
	
	public State() {
		
		content = new DrawingBoard();
		add(content);
		
		JMenuBar menuBar = new JMenuBar(); 
		JMenu editMenu = new JMenu( "Bearbeiten" ); 
		JMenu toolMenu = new JMenu( "Werkzeuge" ); 
		menuBar.add( editMenu ); 
		menuBar.add( toolMenu ); 
		this.setJMenuBar( menuBar );
		
		
		JMenuItem undoItem = new JMenuItem("Zur�ck");
		editMenu.add(undoItem);
		undoItem.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	//UNDO
		    	StackCommands.getInstance().undoCommand();
		    }
		});
		
		JMenuItem redoItem = new JMenuItem("Vor");
		editMenu.add(redoItem);
		redoItem.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	//UNDO
		    	StackCommands.getInstance().redoCommand();
		    }
		});
		
		JMenuItem lines = new JMenuItem("Linien");
		toolMenu.add(lines);
		lines.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	content.setTool(new PenTool(content));
		    }
		});
		
		JMenuItem rect = new JMenuItem("Rechtecke");
		toolMenu.add(rect);
		rect.addActionListener(new ActionListener() {
		    public void actionPerformed( ActionEvent e ) {
		    	content.setTool(new RectTool(content));
		    }
		});

		
		
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Demo Drawing");
        setLocationRelativeTo(null);  // Center window.
       
        pack();

	}
	
	public static void main(String[] args) {
		
	    State f = new State();
		f.setSize(900, 600);
	    f.setVisible( true );

	}

}


