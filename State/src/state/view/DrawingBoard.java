package state.view;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import state.classes.AbstractTool;
import state.classes.Cursor;

import komp.classes.VerbundGrafikObjekt;

public class DrawingBoard extends VerbundGrafikObjekt implements MouseListener, MouseMotionListener {

	private Cursor cursor;

	public DrawingBoard() {
		
		cursor = new Cursor(this);
		
		this.setPreferredSize(new Dimension(1000, 1000));
        this.addMouseMotionListener(this); // Listen to mouse moves, drags.
        this.addMouseListener(this); 
		
	}
	
	public void setTool(AbstractTool tool) {
		cursor.setState(tool);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		//System.out.println("Mouse Moves: "+ e.getX());
		cursor.moveTo(new Point(e.getX(), e.getY()));
	}	
	
	@Override
	public void mouseMoved(MouseEvent e) {
		//System.out.println("Mouse Moves: "+ e.getX());
		cursor.moveTo(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//System.out.println("Mouse Presses: "+ e.getX());
		cursor.mouseDown(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		//System.out.println("Mouse Released: "+ e.getX());
		cursor.mouseUp(new Point(e.getX(), e.getY()));
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}
	

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}
