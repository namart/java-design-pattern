package comand.classes;

import java.util.Stack;


public class StackCommands {
	
	private Stack<ICommand> undoCommands;
	private Stack<ICommand> redoCommands;

	//SingletonClass
	private static final StackCommands instance = new StackCommands();
	public static StackCommands getInstance() {
		return instance;
	}
	
	public StackCommands() {
		undoCommands = new Stack<ICommand>();
		redoCommands = new Stack<ICommand>();
	}
	
	public void addCommand(ICommand command) {
		undoCommands.push(command);
	}
	
	public void undoCommand() {
		if(!undoCommands.isEmpty()) {
			
			ICommand command = undoCommands.pop();
			redoCommands.push(command);
			command.undoExecute();
		}
	}
	
	public void redoCommand() {
		if(!redoCommands.isEmpty()) {
			
			ICommand command = redoCommands.pop();
			undoCommands.push(command);
			command.doExecute();
		}
	}
}
