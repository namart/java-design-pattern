package comand.classes;

import komp.classes.VerbundGrafikObjekt;
import state.objects.Object;

public class PaintCommand implements ICommand {
	
	private Object newObject;
	private Object oldObject;
	private VerbundGrafikObjekt drawingBoard;

	public PaintCommand(VerbundGrafikObjekt drawingBoard, Object object) {
		this.newObject = object;
		this.oldObject = (Object) drawingBoard.getLastInsert();
		this.drawingBoard= drawingBoard;
	}

	@Override
	public void doExecute() {
		drawingBoard.add(newObject);
		drawingBoard.repaint();
	}

	@Override
	public void undoExecute() {
		drawingBoard.remove(oldObject);
		drawingBoard.repaint();
	}

	@Override
	public void redoExecute() {
		drawingBoard.add(newObject);
		drawingBoard.repaint();
	}

}
