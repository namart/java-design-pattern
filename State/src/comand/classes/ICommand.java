package comand.classes;

public interface ICommand {
	public void doExecute();
	public void undoExecute();
	public void redoExecute();
}

