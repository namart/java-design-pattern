package komp.classes;

import java.awt.Graphics;

public interface GrafikObject {

	void draw(Graphics g);

}
