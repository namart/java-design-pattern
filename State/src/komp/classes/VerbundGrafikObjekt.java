package komp.classes;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class VerbundGrafikObjekt extends JPanel implements GrafikObject {
	
	private List<GrafikObject> children = new ArrayList<GrafikObject>();
	private GrafikObject lastInsert = null;
	
	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);    // paints background
		
		//Graphics2D g2d = (Graphics2D)g;
        //g.clearRect(0, 0, this.getWidth(), this.getHeight());
        for (GrafikObject object : children) {
        	object.draw(g);
        }
	}

	@Override
	public void draw(Graphics g) {
		//no implementation needed
	}

	public void add(GrafikObject object) {
		lastInsert = object;
		children.add(object);
	}
	
	public void add(GrafikObject object, int index) {
		lastInsert = object;
		children.add(index, object);
	}
	
	public void remove(GrafikObject object) {
		children.remove(object);
	}
	
	public GrafikObject getGraphicObjectByIndex(int index) {
		return children.get(index);
	}
	
	public int findObject(GrafikObject object) {

		for ( int i = 0; i < children.size(); i++ ) {
			if (children.get(i).equals(object)) {
				return i;
			}
		}
		return -1;

	}

	public List<GrafikObject> getChildren() {
		return children;
	}
	
	public int getChildrenSize() {
		return children.size();
	}
	
	public GrafikObject getLastInsert() {
		return lastInsert;
	}
	
}
